<?php

/**
 * @file
 * Contains Batch API callbacks used during installation.
 *
 * This profile is inspirated from Commerce Kickstart.
 */

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_install_additional_modules()
 */
function _maps_showcase_enable_theme($theme, &$context) {
  theme_enable(array($theme));
  variable_set('theme_default', $theme);

  $context['message'] = st('Installed the default theme.');
}

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_install_additional_modules()
 */
function _maps_showcase_enable_module($module, $module_name, &$context) {
  module_enable(array($module), FALSE);
  $context['message'] = st('Installed %module module.', array('%module' => $module_name));
}

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_install_additional_modules()
 */
function _maps_showcase_setup_localization($operation, &$context) {
  require_once DRUPAL_ROOT . '/includes/language.inc';
  $context['message'] = t('@operation', array('@operation' => $operation));

  // Enable en prefix for english language.
  db_update('languages')
    ->fields(array(
      'prefix' => 'en',
    ))
    ->condition('language',  'en')
    ->execute();

  // Enable language detection via url.
  $negotiation['locale-url'] = array(
    'types' => array(
      'language_content',
      'language',
      'language_url',
    ),
    'callbacks' => array(
      'language' => 'locale_language_from_url',
      'switcher' => 'locale_language_switcher_url',
      'url_rewrite' => 'locale_language_url_rewrite_url',
    ),
    'file' => 'includes/locale.inc',
    'weight' => '-8,',
    'name' => 'URL',
    'description' => t('Determine the language from the URL (Path prefix or domain).'),
    'config' => 'admin/config/regional/language/configure/url',
  );
  language_negotiation_set('language', $negotiation);
}

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_finalize()
 */
function _maps_showcase_taxonomy_menu($operation, $action, $data = array(), &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));

  switch ($action) {
    case 'set_variables':
      list($menu_name, $mlid, $vid) = $data;
      $variable_name = _taxonomy_menu_build_variable('vocab_menu', $vid);
      variable_set($variable_name, $menu_name);
      $variable_name = _taxonomy_menu_build_variable('vocab_parent', $vid);
      variable_set($variable_name, $mlid);
      $variable_name = _taxonomy_menu_build_variable('rebuild', $vid);
      variable_set($variable_name, 1);
      $variable_name = _taxonomy_menu_build_variable('sync', $vid);
      variable_set($variable_name, 1);
      break;

    case 'delete_items':
      $vid = reset($data);
      _taxonomy_menu_delete_all($vid);
      break;

    case 'insert_items':
      list($terms, $menu_name) = $data;

      for ($i = 0, $i_max = count($terms); $i < $i_max; $i++) {
        taxonomy_menu_handler('insert', array(
          'term' => $terms[$i],
          'menu_name' => $menu_name,
        ));
      }
      break;

    case 'menu_rebuild':
      menu_rebuild();
      break;
  }
}

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_finalize()
 */
function _maps_showcase_import($operation, $type, &$context) {
  $context['message'] = t('@operation', array('@operation' => $type));
  $migration =  Migration::getInstance($operation);
  $migration->processImport();
}

/**
 * BatchAPI callback.
 *
 * @see maps_showcase_install_additional_modules()
 */
function _maps_showcase_flush_caches($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));
  drupal_flush_all_caches();
}
