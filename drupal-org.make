; Drupal.org release file.
core = 7.x
api = 2

; Basic contributed modules.
projects[ctools][version] = 1.7
projects[ctools][subdir] = "contrib"
projects[entity][version] = 1.6
projects[entity][subdir] = "contrib"
projects[entityreference][version] = 1.1
projects[entityreference][subdir] = "contrib"
projects[entity_translation][version] = 1.0-beta4
projects[entity_translation][subdir] = "contrib"
projects[libraries][version] = 2.2
projects[libraries][subdir] = "contrib"
projects[relation][version] = 1.0-rc7
projects[relation][subdir] = "contrib"
projects[rules][version] = 2.9
projects[rules][subdir] = "contrib"
projects[token][version] = 1.6
projects[token][subdir] = "contrib"
projects[views][version] = 3.11
projects[views][subdir] = "contrib"
projects[views_bulk_operations][version] = 3.2
projects[views_bulk_operations][subdir] = "contrib"
projects[views_field_view][version] = 1.1
projects[views_field_view][subdir] = "contrib"
projects[xautoload][version] = 5.1
projects[xautoload][subdir] = "contrib"
projects[xautoload][patch][] = "https://www.drupal.org/files/issues/xautoload-base_table_or_view_not-2393205-16.patch"
projects[xautoload][patch][] = "https://www.drupal.org/files/issues/test_fails_since_psr_4-2479507-1.patch"

; Field related modules.
projects[addressfield][version] = 1.1
projects[addressfield][subdir] = "contrib"
projects[date][version] = 2.8
projects[date][subdir] = "contrib"
projects[link][version] = 1.3
projects[link][subdir] = "contrib"
projects[title][version] = 1.0-alpha7
projects[title][subdir] = "contrib"
projects[title][patch][] = "http://drupal.org/files/title-translation_overwrite-1269076-35.patch"

; Features related modules.
projects[features][version] = 2.5
projects[features][subdir] = "contrib"
projects[features_extra][version] = 1.0-beta1
projects[features_extra][subdir] = "contrib"
projects[migrate][version] = 2.7
projects[migrate][subdir] = "contrib"
projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"

; UI improvement modules.
projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][subdir] = "contrib"
projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = "contrib"
projects[menu_block][version] = 2.5
projects[menu_block][subdir] = "contrib"
projects[menu_position][version] = 1.1
projects[menu_position][subdir] = "contrib"
projects[module_filter][version] = 2.0
projects[module_filter][subdir] = "contrib"
projects[panels][version] = 3.5
projects[panels][subdir] = "contrib"
projects[taxonomy_menu][version] = 1.5
projects[taxonomy_menu][subdir] = "contrib"

; File system.
projects[file_entity][version] = 2.0-beta1
projects[file_entity][subdir] = "contrib"
projects[remote_stream_wrapper][version] = 1.0-rc1
projects[remote_stream_wrapper][subdir] = "contrib"

; Internationalization modules.
projects[variable][version] = 2.5
projects[variable][subdir] = "contrib"
projects[i18n][version] = 1.13
projects[i18n][subdir] = "contrib"
projects[l10n_client][version] = 1.3
projects[l10n_client][subdir] = "contrib"
projects[l10n_update][version] = 2.0
projects[l10n_update][subdir] = "contrib"

; Search related modules.
projects[search_api][version] = 1.14
projects[search_api][subdir] = "contrib"
projects[search_api_db][version] = 1.4
projects[search_api_db][subdir] = "contrib"
projects[facetapi][version] = 1.5
projects[facetapi][subdir] = "contrib"
projects[facetapi][patch][] = "http://drupal.org/files/facetapi-1616518-13-show-active-term.patch"

; User experience modules.
projects[bxslider_views_slideshow][version] = 1.50
projects[bxslider_views_slideshow][subdir] = "contrib"
projects[colorbox][version] = 2.8
projects[colorbox][subdir] = "contrib"
projects[eva][version] = 1.2
projects[eva][subdir] = "contrib"
projects[jquery_update][version] = 3.0-alpha2
projects[jquery_update][subdir] = "contrib"
projects[service_links][version] = 2.2
projects[service_links][subdir] = "contrib"
projects[views_megarow][version] = 1.4
projects[views_megarow][subdir] = "contrib"
projects[views_slideshow][version] = 3.1
projects[views_slideshow][subdir] = "contrib"

; Content editing modules.
projects[better_formats][version] = 1.0-beta1
projects[better_formats][subdir] = "contrib"
projects[linkit][version] = 3.3
projects[linkit][subdir] = "contrib"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][subdir] = "contrib"

; Email related modules.
projects[mailsystem][version] = 2.34
projects[mailsystem][subdir] = "contrib"
projects[mimemail][version] = 1.0-beta3
projects[mimemail][subdir] = "contrib"

; SEO modules.
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = "contrib"

; MaPS System modules.
projects[maps_suite][version] = 2.x-dev
projects[maps_suite][subdir] = "contrib"

; Themes.
projects[maps_admin][version] = 1.4
projects[maps_theme_commerce][version] = 1.0

; Libraries.
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[colorbox][type] = "libraries"
libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"
libraries[jquery.bxslider][type] = "libraries"
libraries[jquery.bxslider][download][type] = "file"
libraries[jquery.bxslider][download][url] = "https://github.com/stevenwanderski/bxslider-4/archive/master.zip"
libraries[jquery.cycle][type] = "libraries"
libraries[jquery.cycle][download][type] = "file"
libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle/archive/master.zip"
libraries[jquery.mousewheel][type] = "libraries"
libraries[jquery.mousewheel][download][type] = "git"
libraries[jquery.mousewheel][download][tag] = 3.1.4
libraries[jquery.mousewheel][download][url] = "https://github.com/brandonaaron/jquery-mousewheel.git"
libraries[json2][type] = "libraries"
libraries[json2][download][type] = "file"
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js/archive/master.zip"