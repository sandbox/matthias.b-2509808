<?php
/**
 * @file
 * maps_showcase_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function maps_showcase_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function maps_showcase_content_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'flat_product' => array(
      'name' => t('Flat product'),
      'base' => 'node_content',
      'description' => t('A <em>Flat Product</em> content displays a Showcase product that has no variation.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'marketing_product' => array(
      'name' => t('Marketing product'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
