<?php

/**
 * @file
 *  Contains base migration classes used by MaPS Showcase demo products.
 */

/**
 * Common features for all the migrations.
 */
abstract class MapsShowcaseMigration extends Migration {

  public function processImport(array $options = array()) {
    parent::processImport($options);
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }

}

class MapsShowcaseHighlights extends MapsShowcaseMigration {

  public function __construct($arguments = array()) {
    parent::__construct($arguments);
    $this->description = t('Import highlight nodes.');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'maps_showcase_migrate') . '/import/highlights.csv', $this->csvcolumns(), array('header_rows' => 1));

    $this->destination = new MigrateDestinationNode('highlight');

    $this->addFieldMapping('title', 'title');

    $this->addFieldMapping('uid', 'uid')->defaultValue(1);
    $this->addFieldMapping('is_new', 'is_new')->defaultValue(TRUE);
    $this->addFieldMapping('language', 'language');

    // Images
    $this->addFieldMapping('field_highlight_image', 'image');
    $this->addFieldMapping('field_highlight_image:file_replace')
      ->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_highlight_image:source_dir')
      ->defaultValue(drupal_get_path('module', 'maps_showcase_migrate') . '/import/images');

  }

  function csvcolumns() {
    $columns[0] = array('title', 'Title');
    $columns[1] = array('image', 'Image');
    return $columns;
  }

  function prepareRow($row) {
    $row->language = LANGUAGE_NONE;
  }

}
