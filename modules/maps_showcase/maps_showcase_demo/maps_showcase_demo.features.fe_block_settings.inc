<?php
/**
 * @file
 * maps_showcase_demo.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function maps_showcase_demo_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_showcase' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_showcase',
        'weight' => -11,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 3,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_showcase' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_showcase',
        'weight' => -13,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 2,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_showcase' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_showcase',
        'weight' => -13,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
