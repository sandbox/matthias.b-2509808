<?php
/**
 * @file
 * maps_showcase_demo.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function maps_showcase_demo_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__marketing_product';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Marketing product',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'marketing_product',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'marketing_product' => 'marketing_product',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '4ab5902d-3f46-414d-977c-ee10e5bd2181';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-37c61b62-ecf1-44de-ab1e-ffed8683c45a';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:title_field';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'title_linked',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => '_none',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '37c61b62-ecf1-44de-ab1e-ffed8683c45a';
    $display->content['new-37c61b62-ecf1-44de-ab1e-ffed8683c45a'] = $pane;
    $display->panels['middle'][0] = 'new-37c61b62-ecf1-44de-ab1e-ffed8683c45a';
    $pane = new stdClass();
    $pane->pid = 'new-8ef93969-2808-4678-bf51-4368a34b380d';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-related_products_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8ef93969-2808-4678-bf51-4368a34b380d';
    $display->content['new-8ef93969-2808-4678-bf51-4368a34b380d'] = $pane;
    $display->panels['middle'][1] = 'new-8ef93969-2808-4678-bf51-4368a34b380d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-37c61b62-ecf1-44de-ab1e-ffed8683c45a';
  $handler->conf['display'] = $display;
  $export['node_view__marketing_product'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Flat product',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'flat_product' => 'flat_product',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1d8405e5-8b92-4afe-8a98-703c7e8ca93b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1cdf285f-656a-4ab5-a065-77fcd7c0a843';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-images_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1cdf285f-656a-4ab5-a065-77fcd7c0a843';
    $display->content['new-1cdf285f-656a-4ab5-a065-77fcd7c0a843'] = $pane;
    $display->panels['middle'][0] = 'new-1cdf285f-656a-4ab5-a065-77fcd7c0a843';
    $pane = new stdClass();
    $pane->pid = 'new-8ff9d264-8b8c-4ce4-9a91-4faff0abcd13';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:title_field';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'title_linked',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => 'h1',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8ff9d264-8b8c-4ce4-9a91-4faff0abcd13';
    $display->content['new-8ff9d264-8b8c-4ce4-9a91-4faff0abcd13'] = $pane;
    $display->panels['middle'][1] = 'new-8ff9d264-8b8c-4ce4-9a91-4faff0abcd13';
    $pane = new stdClass();
    $pane->pid = 'new-356f21ef-1223-4f48-8a8a-88566d496cce';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_reference';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'title_linked',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => '_none',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '356f21ef-1223-4f48-8a8a-88566d496cce';
    $display->content['new-356f21ef-1223-4f48-8a8a-88566d496cce'] = $pane;
    $display->panels['middle'][2] = 'new-356f21ef-1223-4f48-8a8a-88566d496cce';
    $pane = new stdClass();
    $pane->pid = 'new-6066e156-233b-4e4d-bc3f-b3c1833008db';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:description_field';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '6066e156-233b-4e4d-bc3f-b3c1833008db';
    $display->content['new-6066e156-233b-4e4d-bc3f-b3c1833008db'] = $pane;
    $display->panels['middle'][3] = 'new-6066e156-233b-4e4d-bc3f-b3c1833008db';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'search_node_panel_context';
  $handler->task = 'search';
  $handler->subtask = 'node';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Products',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '0e9f62c1-d093-4302-9619-202636e03745';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-08a9e991-8b60-40f5-ac63-02523a2307ed';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Title',
      'title' => '',
      'body' => 'Results for %keywords:raw',
      'format' => 'filtered_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'page-title',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '08a9e991-8b60-40f5-ac63-02523a2307ed';
    $display->content['new-08a9e991-8b60-40f5-ac63-02523a2307ed'] = $pane;
    $display->panels['middle'][0] = 'new-08a9e991-8b60-40f5-ac63-02523a2307ed';
    $pane = new stdClass();
    $pane->pid = 'new-f66737ec-e17e-4494-93d6-c13edb7d8cfb';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-search_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f66737ec-e17e-4494-93d6-c13edb7d8cfb';
    $display->content['new-f66737ec-e17e-4494-93d6-c13edb7d8cfb'] = $pane;
    $display->panels['middle'][1] = 'new-f66737ec-e17e-4494-93d6-c13edb7d8cfb';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-f66737ec-e17e-4494-93d6-c13edb7d8cfb';
  $handler->conf['display'] = $display;
  $export['search_node_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'search_node_panel_context_2';
  $handler->task = 'search';
  $handler->subtask = 'node';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Products',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7b9ee325-b42a-41ba-be63-2fa3321ec6e6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-270d0b09-b525-412a-8979-4c10a2b4548e';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Title',
      'title' => '',
      'body' => 'Results for %keywords:raw',
      'format' => 'filtered_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'page-title',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '270d0b09-b525-412a-8979-4c10a2b4548e';
    $display->content['new-270d0b09-b525-412a-8979-4c10a2b4548e'] = $pane;
    $display->panels['middle'][0] = 'new-270d0b09-b525-412a-8979-4c10a2b4548e';
    $pane = new stdClass();
    $pane->pid = 'new-8dca1f8b-8ba0-4ebc-b7d3-181909661d3e';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-search_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8dca1f8b-8ba0-4ebc-b7d3-181909661d3e';
    $display->content['new-8dca1f8b-8ba0-4ebc-b7d3-181909661d3e'] = $pane;
    $display->panels['middle'][1] = 'new-8dca1f8b-8ba0-4ebc-b7d3-181909661d3e';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-8dca1f8b-8ba0-4ebc-b7d3-181909661d3e';
  $handler->conf['display'] = $display;
  $export['search_node_panel_context_2'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view_panel_context';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Category',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_bundle:taxonomy_term',
          'settings' => array(
            'type' => array(
              'category' => 'category',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '27fd8993-28df-484e-ac61-c7d058e5ad83';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6d696262-e1fa-4bad-a6d9-c6801b57f93b';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Category name',
      'title' => '',
      'body' => '%term:name',
      'format' => 'filtered_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'page-title',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6d696262-e1fa-4bad-a6d9-c6801b57f93b';
    $display->content['new-6d696262-e1fa-4bad-a6d9-c6801b57f93b'] = $pane;
    $display->panels['middle'][0] = 'new-6d696262-e1fa-4bad-a6d9-c6801b57f93b';
    $pane = new stdClass();
    $pane->pid = 'new-ed87d653-d8f8-4a83-a784-fdbbd5a856d3';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-products_by_category_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ed87d653-d8f8-4a83-a784-fdbbd5a856d3';
    $display->content['new-ed87d653-d8f8-4a83-a784-fdbbd5a856d3'] = $pane;
    $display->panels['middle'][1] = 'new-ed87d653-d8f8-4a83-a784-fdbbd5a856d3';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-ed87d653-d8f8-4a83-a784-fdbbd5a856d3';
  $handler->conf['display'] = $display;
  $export['term_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function maps_showcase_demo_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'Home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'de12cd22-f9fb-4607-a05b-8e40ba97cccf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-072f9da9-4a62-4f36-9c88-562695166005';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'highlights-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '072f9da9-4a62-4f36-9c88-562695166005';
    $display->content['new-072f9da9-4a62-4f36-9c88-562695166005'] = $pane;
    $display->panels['middle'][0] = 'new-072f9da9-4a62-4f36-9c88-562695166005';
    $pane = new stdClass();
    $pane->pid = 'new-d539859f-50d9-4ab1-9fe1-cd56c36bd32c';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-promoted_products';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'd539859f-50d9-4ab1-9fe1-cd56c36bd32c';
    $display->content['new-d539859f-50d9-4ab1-9fe1-cd56c36bd32c'] = $pane;
    $display->panels['middle'][1] = 'new-d539859f-50d9-4ab1-9fe1-cd56c36bd32c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  return $pages;

}
