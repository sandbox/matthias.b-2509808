<?php
/**
 * @file
 * maps_showcase_demo.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function maps_showcase_demo_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-highlight-field_highlight_image'
  $field_instances['node-highlight-field_highlight_image'] = array(
    'bundle' => 'highlight',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => '',
    'field_name' => 'field_highlight_image',
    'label' => 'highlight_image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'images/highlight',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '2400x1600',
      'min_resolution' => '972x310',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('highlight_image');

  return $field_instances;
}
