<?php
/**
 * @file
 * maps_showcase_demo.maps_import_profile_default.inc
 */

/**
 * Implements hook_maps_import_default_profiles().
 */
function maps_showcase_demo_maps_import_default_profiles() {
  $export = array();

  $profile = Drupal\maps_import\Plugins\CTools\ExportUI\ProfileExportable::__set_state(array(
    'disabled' => FALSE, /* Edit this to true to make a default profile disabled initially */
    'api_version' => 1,
    'languages' => array(
        1 => '',
        2 => 'en',
        3 => '',
      ),
    'statuses' => array(
        1 => array(
          'status' => '0',
          'weight' => '0',
        ),
        2 => array(
          'status' => '0',
          'weight' => '0',
        ),
        4 => array(
          'status' => '0',
          'weight' => '0',
        ),
        8 => array(
          'status' => '1',
          'weight' => '0',
        ),
      ),
    'media_types' => array(
        1 => array(
          'media_types' => '0',
        ),
      ),
    'name' => 'maps_demo',
    'title' => 'MaPS demo',
    'fetch_method' => 'ws',
    'token' => 'aec00ac4c43a3bf513cda6529b1a1323a1dec593',
    'publication_id' => '975',
    'root_object_id' => '976',
    'preset_group_id' => '1383209175',
    'url' => 'http://demo.maps-system.com/private/ws/mapsWebService.php',
    'media_directory' => 'maps_suite',
    'media_accessibility' => 'public',
    'max_objects_per_request' => '50',
    'max_objects_per_op' => '50',
    'format' => 'xml',
    'enabled' => TRUE,
    'weight' => '0',
    'web_template' => 'export_web',
    'options' => array(
        'media_settings' => array(
          1 => array(
            'path' => 'images',
            'preset' => '1383209433',
          ),
          2 => array(
            'path' => 'documents',
            'preset' => '0',
          ),
        ),
      ),
    'objects_file' => '',
    'configuration_file' => '',
    'converters' => array(
      Drupal\maps_import\Plugins\CTools\ExportUI\ConverterExportable::__set_state(array(
        'class' => 'Drupal\\maps_import\\Converter\\Object',
        'name' => 'category',
        'title' => 'Category',
        'description' => 'Categories as defined by MaPS System.',
        'uid' => 'property:code',
        'uid_scope' => 1,
        'entity_type' => 'taxonomy_term',
        'bundle' => 'category',
        'options' => array(),
        'weight' => '-50',
        'conditions' => array(
          array(
            'type' => 'config_type',
            'extra' => array(
              'criteria' => 'none',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\ConfigurationType',
          ),
          array(
            'type' => 'object_nature',
            'extra' => array(
              'criteria' => '2',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\Nature',
          ),
        ),
        'mapping_items' => array(
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_code',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:parent_id',
            'field_name' => 'parent',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'attribute:1',
            'field_name' => 'name',
            'static' => 0,
            'required' => TRUE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'media:1',
            'field_name' => 'field_image',
            'static' => 0,
            'required' => FALSE,
            'weight' => '50',
            'options' => array(
              'media_type' => '1',
              'media_start_range' => '1',
              'media_limit_range' => '5',
            ),
            'type' => 'object',
          ),
        ),
      )),
      Drupal\maps_import\Plugins\CTools\ExportUI\ConverterExportable::__set_state(array(
        'class' => 'Drupal\\maps_import\\Converter\\Object',
        'name' => 'flat_article',
        'title' => 'Flat article',
        'description' => 'A <em>flat article</em> as defined by MaPS System.',
        'uid' => 'property:code',
        'uid_scope' => 1,
        'entity_type' => 'node',
        'bundle' => 'flat_product',
        'options' => array(
        'status' => 'unpublish',
      ),
        'weight' => '-49',
        'conditions' => array(
          array(
            'type' => 'config_type',
            'extra' => array(
              'criteria' => 'none',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\ConfigurationType',
          ),
          array(
            'type' => 'object_nature',
            'extra' => array(
              'criteria' => '6',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\Nature',
          ),
        ),
        'mapping_items' => array(
          array(
            'property_id' => 'attribute:1',
            'field_name' => 'title_field',
            'static' => 0,
            'required' => TRUE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:parent_id',
            'field_name' => 'field_category',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_reference',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'attribute:102',
            'field_name' => 'promote',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'attribute:27',
            'field_name' => 'description_field',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(
              'target' => array(
                'format' => 'full_html',
              ),
            ),
            'type' => 'object',
          ),
          array(
            'property_id' => 'media:1',
            'field_name' => 'field_image',
            'static' => 0,
            'required' => FALSE,
            'weight' => '50',
            'options' => array(
              'media_type' => '1',
              'media_start_range' => '1',
              'media_limit_range' => '5',
            ),
            'type' => 'object',
          ),
        ),
      )),
      Drupal\maps_import\Plugins\CTools\ExportUI\ConverterExportable::__set_state(array(
        'class' => 'Drupal\\maps_import\\Converter\\Object',
        'name' => 'marketing_product',
        'title' => 'Marketing product',
        'description' => 'A marketing product as defined by MaPS System.',
        'uid' => 'property:code',
        'uid_scope' => 1,
        'entity_type' => 'node',
        'bundle' => 'marketing_product',
        'options' => array(
        'status' => 'unpublish',
      ),
        'weight' => '0',
        'conditions' => array(
          array(
            'type' => 'config_type',
            'extra' => array(
              'criteria' => 'none',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\ConfigurationType',
          ),
          array(
            'type' => 'object_nature',
            'extra' => array(
              'criteria' => '3',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\Nature',
          ),
        ),
        'mapping_items' => array(
          array(
            'property_id' => 'property:parent_id',
            'field_name' => 'field_category',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'attribute:1',
            'field_name' => 'title_field',
            'static' => 0,
            'required' => TRUE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_code',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_reference',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'attribute:102',
            'field_name' => 'promote',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'media:1',
            'field_name' => 'field_image',
            'static' => 0,
            'required' => FALSE,
            'weight' => '50',
            'options' => array(
              'media_type' => '1',
              'media_start_range' => '1',
              'media_limit_range' => '5',
            ),
            'type' => 'object',
          ),
        ),
      )),
      Drupal\maps_import\Plugins\CTools\ExportUI\ConverterExportable::__set_state(array(
        'class' => 'Drupal\\maps_import\\Converter\\Object',
        'name' => 'article',
        'title' => 'Article',
        'description' => 'An article as defined by MaPS System.',
        'uid' => 'property:code',
        'uid_scope' => 1,
        'entity_type' => 'node',
        'bundle' => 'article',
        'options' => array(),
        'weight' => '0',
        'conditions' => array(
          array(
            'type' => 'config_type',
            'extra' => array(
              'criteria' => 'none',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\ConfigurationType',
          ),
          array(
            'type' => 'object_nature',
            'extra' => array(
              'criteria' => '4',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Object\\Nature',
          ),
        ),
        'mapping_items' => array(
          array(
            'property_id' => 'attribute:1',
            'field_name' => 'title_field',
            'static' => 0,
            'required' => TRUE,
            'weight' => '-50',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'media:1',
            'field_name' => 'field_image',
            'static' => 0,
            'required' => FALSE,
            'weight' => '-49',
            'options' => array(
              'media_type' => '1',
              'media_start_range' => '1',
              'media_limit_range' => '5',
            ),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_code',
            'static' => 0,
            'required' => FALSE,
            'weight' => '-48',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:code',
            'field_name' => 'field_reference',
            'static' => 0,
            'required' => FALSE,
            'weight' => '-47',
            'options' => array(),
            'type' => 'object',
          ),
          array(
            'property_id' => 'property:parent_id',
            'field_name' => 'field_related_product',
            'static' => 0,
            'required' => FALSE,
            'weight' => '-46',
            'options' => array(),
            'type' => 'object',
          ),
        ),
      )),
      Drupal\maps_import\Plugins\CTools\ExportUI\ConverterExportable::__set_state(array(
        'class' => 'Drupal\\maps_import\\Converter\\Media',
        'name' => 'images',
        'title' => 'Images',
        'description' => '',
        'uid' => 'property:id',
        'uid_scope' => 1,
        'entity_type' => 'file',
        'bundle' => 'image',
        'options' => array(
        'file_management' => 'download',
      ),
        'weight' => '0',
        'conditions' => array(
          array(
            'type' => 'media_type',
            'extra' => array(
              'criteria' => '1',
              'negate' => NULL,
            ),
            'weight' => '0',
            'class' => 'Drupal\\maps_import\\Filter\\Condition\\Leaf\\Media\\Type',
          ),
        ),
        'mapping_items' => array(
          array(
            'property_id' => 'attribute:7',
            'field_name' => 'field_file_image_title_text',
            'static' => 0,
            'required' => FALSE,
            'weight' => '0',
            'options' => array(),
            'type' => 'object',
          ),
        ),
      )),
    ),
  ));

  $export['maps_demo'] = $profile;

  return $export;
}
