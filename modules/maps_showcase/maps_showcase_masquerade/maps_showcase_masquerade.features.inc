<?php
/**
 * @file
 * maps_showcase_masquerade.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function maps_showcase_masquerade_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
