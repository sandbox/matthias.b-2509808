<?php
/**
 * @file
 * maps_showcase_masquerade.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function maps_showcase_masquerade_user_default_roles() {
  $roles = array();

  // Exported role: masquerade.
  $roles['masquerade'] = array(
    'name' => 'masquerade',
    'weight' => 3,
  );

  return $roles;
}
